# By Erick Siordia Nagaya, ID 157504

import cv2 as cv
import random
import tkinter as tk
from functools import reduce


# Definition of the Individual class.
class Individual:
    # Constructor
    def __init__(self, genome, path):
        # Initialization of the attributes. In this case, the attributes are the genes
        # of the individual, which represent the set of parameters and filters to apply to an image.
        self.heuristicValue = -1
        self.blur1 = genome[0]
        self.blur2 = genome[1]
        self.blur3 = genome[2]
        self.threshold = genome[3]
        self.accumulator = genome[4]
        self.imagePath = path

        # The actual observations are the coordinates of the centers of the circles in the image.
        # We need them so the algorithm can calculate an heuristic value in base of the detected
        # circles, and the actual circles in the image. This coordinates were obtained manually.
        self.actual_observations = self.get_actual_observations()
        self.heuristicValue = self.calculate_heuristic_value()

    def get_heuristic_value(self):
        return self.heuristicValue

    # This method applies all the filters to the image with the parameters specified by the genome of the individual,
    # and draws circles where the HoughCircle transform detects circles. It returns the image with the circle drawings.
    def detect_coins(self, image_path):
        image = cv.imread(image_path, 0)
        image = cv.medianBlur(image, self.blur1)
        image = cv.GaussianBlur(image, (self.blur2[0], self.blur2[1]), 0)
        image = cv.bilateralFilter(image, self.blur3[0], self.blur3[1], self.blur3[2])

        circles = cv.HoughCircles(image, cv.HOUGH_GRADIENT, 1, 18, param1=self.threshold, param2=self.accumulator,
                                  minRadius=25, maxRadius=45)

        sketch = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

        for circle in circles[0, :]:
            cv.circle(sketch, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)
            cv.circle(sketch, (circle[0], circle[1]), 2, (0, 0, 255), 3)

        return sketch

    # This method returns a list of genes of and individual
    def get_genes(self):
        return [self.blur1, self.blur2, self.blur3, self.threshold, self.accumulator]

    # This method returns a list containing the coordinates of the circles in each image.
    def get_actual_observations(self):
        if "circles.png" in self.imagePath:
            observations = [[81, 176], [130, 42], [146, 198],
                            [151, 258], [176, 296], [150, 340],
                            [199, 107], [216, 106], [251, 8],
                            [273, 52], [244, 166], [229, 231],
                            [259, 217], [271, 280], [303, 337],
                            [330, 135], [347, 116], [313, 192],
                            [366, 224], [327, 295], [373, 291],
                            [409, 264], [421, 346], [433, 165],
                            [420, 87], [446, 67]]
        elif "coinsBig.png" in self.imagePath:
               observations = [[658, 252], [747, 526], [565, 832], [585, 1107],
                                [942, 842], [842, 1298], [1038, 1534], [1290, 1402],
                                [1181, 1150], [1250, 897], [1075, 653], [1242, 503],
                                [1385, 320], [1161, 200], [1708, 428], [1659, 648],
                                [1799, 851], [1598, 1001], [1842, 1205], [1506, 1262],
                                [1599, 1511]]
        elif "coins.png" in self.imagePath:
               observations = [[249, 106], [283, 200], [215, 316], [357, 320],
                                [407, 248], [469, 190], [525, 121], [441, 77],
                                [647, 162], [628, 246], [477, 337], [222, 421],
                                [319, 491], [447, 436], [391, 581], [488, 531],
                                [570, 478], [606, 378], [606, 674], [697, 456],
                                [683, 321]]
        else:
            observations = [[160, 136], [303, 137], [235, 240], [383, 256], [389, 362],
                            [224, 446], [337, 455], [273, 336], [160, 367], [92, 298]]

        return observations

    # This method verifies if the point passed as parameter is within 5 pixels of one of the actual circles coordinates.
    def positive(self, point, ls):
        x, y = point
        m = list(map((lambda observation: int(x) in range(int(observation[0]) - 5, int(observation[0] + 5)) and int(y) in range (int(observation[1]) - 5, int(observation[1]) + 5)), ls))
        r = reduce((lambda a, b: a or b), m)
        return r

    # This method counts the true positives (circles detected that are in the image),
    # false positives (circles detected that don't exist),
    # and false negatives (circles not detected where there should be a circle).
    def calculate_heuristic_value(self):
        image = cv.imread(self.imagePath, 0)
        image = cv.medianBlur(image, self.blur1)
        image = cv.GaussianBlur(image, (self.blur2[0], self.blur2[1]), 0)
        image = cv.bilateralFilter(image, self.blur3[0], self.blur3[1], self.blur3[2])

        try:
            circles = cv.HoughCircles(image, cv.HOUGH_GRADIENT, 1, 18, param1=self.threshold, param2=self.accumulator,
                              minRadius=25, maxRadius=45)
            TP = FP = 0

            for circle in circles[0, :]:
                if self.positive(circle[:-1], self.actual_observations):
                    TP += 1
                else:
                    FP += 1
            FN = len(self.actual_observations) - TP

            self.heuristicValue = self.get_fscore(TP, FP, FN)
        except:
            self.heuristicValue = 0

        return self.heuristicValue

    # This method calculates the heuristic value of the inidividual.
    # in this case, the heuristic value is represented by the fscore, which is a ratio
    # between the true positives (circles detected that are in the image), false positives
    # (circles detected that don't exist), and false negatives (circles not detected where there should be a circle).
    def get_fscore(self, TP, FP, FN):
        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        fscore = 2 * (recall * precision) / (recall + precision) if TP > 0 else 0
        return fscore


# Definition of the Generation class.
class Generation:
    # Constructor
    def __init__(self, size, path):
        if size == 0:
            self.individuals = list()
        elif size > 0:
            self.individuals = self.generate_individuals(size, path)
        else:
            print("The generation can only have a positive size number size.")

    # This method returns a list of n individuals associated with an image (to calculate heuristic value).
    @staticmethod
    def generate_individuals(number, image_path):
        individuals = list()
        print("Generating initial individuals", end="")
        for i in range(0, number):
            gens = PopulationUtils.generate_genome()
            individuals.append(Individual(gens, image_path))
            print(".", end="")
        print("")
        return individuals

    # This method returns the current individuals in the generation.
    def get_individuals(self):
        inds = self.individuals
        return inds

    # This method returns the individual with the highest heuristic value from a group of n
    # random individuals.
    def tournament(self, size):
        candidates = list()
        pop_size = len(self.individuals)

        for i in range(size):
            selected_index = random.randint(0, pop_size-1)
            selected_individual = self.individuals[selected_index]
            candidates.append(selected_individual)

        fittest_individual = self.get_best_individual(candidates)

        return fittest_individual

    # This method returns the individual with the highest heuristic value from a group of individuals passed
    # as parameter.
    def get_best_individual(self, individuals):
        best_individual = None
        best_score = 0

        for ind in individuals:
            individual_value = ind.get_heuristic_value()
            if individual_value > best_score:
                best_individual = ind
                best_score = individual_value

        if best_individual is None:
            best_individual = individuals[random.randint(0, len(individuals) - 1)]

        return best_individual

    # This method append an individual to the current generation.
    def append_individual(self, individual):
        self.individuals.append(individual)

# Definition of the abstract class PopulationUtils
class PopulationUtils:
    # This method returns list containing a random set of values to use as a genome in an individual.
    @staticmethod
    def generate_genome():
        blur1 = random.randrange(1, 11, 2)
        blur2 = [random.randrange(1, 11, 2), random.randrange(1, 7, 2)]
        blur3 = [random.randint(1, 5), random.randint(1, 5), random.randint(1, 5)]
        threshold = random.randint(1, 200)
        accumulator = random.randint(1, 200)

        return [blur1, blur2, blur3, threshold, accumulator]

    # This method takes an initial generation, a number of generations, a tournament size, an elitism variable,
    # an image path and a text widget to evolve the population passed as parameter. It returns the last
    # generation in the process of evolution.
    @staticmethod
    def evolve(initial_generation, number_of_generations, tournament_size, elitism, imagePath, text):
        current_generation = initial_generation
        population_size = len(initial_generation.get_individuals())

        for i in range(number_of_generations):
            text.insert(tk.END, "Generating generation {}\n".format(i + 1))
            new_generation = Generation(size=0, path=imagePath)

            for j in range(population_size):
                if j == 0 and elitism == 1:
                    current_generation.get_best_individual(current_generation.get_individuals())

                father = current_generation.tournament(tournament_size)
                mother = current_generation.tournament(tournament_size)
                child = PopulationUtils.crossover(father, mother)
                new_generation.append_individual(child)

            current_generation = new_generation
            del new_generation

            last_population = current_generation.get_individuals()
            best_generation_individual = current_generation.get_best_individual(last_population)
            best_genes = best_generation_individual.get_genes()
            text.insert(tk.END, "\n")
            text.insert(tk.END,
                "\tBest values in generation: blur1 = {}; blur2 = {},{}; blur3 = {},{},{}; threshold = {}; accumulator = {}\n".format(
                    best_genes[0], best_genes[1][0], best_genes[1][1],
                    best_genes[2][0], best_genes[2][1], best_genes[2][2],
                    best_genes[3], best_genes[4]))
            text.insert(tk.END, "\tBest fscore in generation {} : {:0.3f}\n".format(i + 1, best_generation_individual.get_heuristic_value()))
            text.insert(tk.END, "============================\n")

        return current_generation

    # This method implements the crossover of two individuals.
    # Each gene has 50% of chance of choosing the father or mother gene.
    @staticmethod
    def crossover(father, mother):
        child_genes = list()
        father_genes = father.get_genes()
        mother_genes = mother.get_genes()

        for i in range(5):
            crossover_factor = random.randint(0, 9)
            if crossover_factor > 5:
                child_genes.append(mother_genes[i])
            else:
                child_genes.append(father_genes[i])

        new_genome = PopulationUtils.generate_genome()
        new_individual = Individual(new_genome, father.imagePath)

        return new_individual


# Definition of the UI class.
class UI:
    # Constructor
    def __init__(self):
        main_window = tk.Tk()
        main_window.title("Genetic algorithm for circle detection")
        main_window.geometry("930x385+8+400")
        self.lastResult = None

        self.elitismValue = tk.IntVar()

        left_frame = tk.Frame(main_window)
        left_frame.grid(row=0, column=0)

        right_frame = tk.Frame(main_window)
        right_frame.grid(row=0, column=1)

        label_frame = tk.LabelFrame(right_frame, text="Parameters", font=("Helvetica", 16))
        label_frame.grid(row=1, column=0, columnspan=2)

        # Creation of labels and text fields for the GUI-
        path_label = tk.Label(label_frame, text="Image path:")
        path_label.grid(row=2, column=0)
        self.path = tk.StringVar()
        self.path_field = tk.Entry(label_frame, textvariable = self.path)
        self.path_field.grid(row=2, column=1)

        pop_size_label = tk.Label(label_frame, text="Population size:")
        pop_size_label.grid(row=3, column=0)
        self.pop_size_field = tk.Entry(label_frame)
        self.pop_size_field.grid(row=3, column=1)

        num_of_gens_label = tk.Label(label_frame, text="Number of generations:")
        num_of_gens_label.grid(row=4, column=0)
        self.num_of_gens_field = tk.Entry(label_frame)
        self.num_of_gens_field.grid(row=4, column=1)

        tnmt_size_label = tk.Label(label_frame, text="Tournament size:")
        tnmt_size_label.grid(row=5, column=0)
        self.tnmt_size_field = tk.Entry(label_frame)
        self.tnmt_size_field.grid(row=5, column=1)

        elitism_check = tk.Checkbutton(label_frame, text="Elitism", variable=self.elitismValue, onvalue=1, offvalue=0)
        elitism_check.grid(row=6, column=1)

        scrollbar = tk.Scrollbar(left_frame)
        scrollbar.grid(row=0, column=1, rowspan=2, sticky="ns")

        # Creation of the text area widget.
        self.text = tk.Text(left_frame, bg="black", fg="white", yscrollcommand=scrollbar.set)
        self.text.insert(tk.END, "Instructions:\n1. Create a population and evolve it with one of the images available for training (coins.png or circles.png)\n2. Press Recognize circles button to show the image with the detected circles. You can load another image to detect circles if you want.")
        self.text.insert(tk.END, "\nNote: The program may take a while to process the image depending on the parameters given.\nPLEASE WAIT AND DONT CLOSE THE PROGRAM.\n")
        self.text.grid(row=1, column=0)

        # Creation of the start and clear button.
        start_button = tk.Button(right_frame, text="Start evolution", command=self.start_algorithm)
        start_button.grid(row=2, column=0, columnspan=2)
        delete_button = tk.Button(right_frame, text="Clear console", command=self.clear)
        delete_button.grid(row=4, column=0, columnspan = 2)
        self.recognize_button = tk.Button(right_frame, text="Recognize circles", command=self.detect_with_last_result)
        self.recognize_button.grid(row=3, column=0, columnspan=2)
        self.recognize_button.config(state=tk.DISABLED)

        main_window.resizable(False, False)

        main_window.mainloop()

    def detect_with_last_result(self):
        if self.lastResult is not None:
            image_path = str(self.path.get())
            circles = self.lastResult.detect_coins(image_path)

            cv.imshow("Detected circles", circles)
            cv.waitKey(0)
            cv.destroyAllWindows()
        else:
            print("You first need to create and evolve a population!")
            self.text.insert(tk.END, "You first need to create and evolve a population!")


    # Method to call when the start button is pressed.
    def start_algorithm(self):
        # Getting the values from the GUI
        population_size = int(self.pop_size_field.get())
        number_of_generations = int(self.num_of_gens_field.get())
        tournament_size = int(self.tnmt_size_field.get())
        elitism = int(self.elitismValue.get())
        image_path = str(self.path.get())

        self.text.insert(tk.END, "Starting the evolution of the individuals...\n")

        try:
            initial_population = Generation(population_size, image_path)
            last_generation = PopulationUtils.evolve(initial_population, number_of_generations, tournament_size, elitism, image_path, self.text)
            best_individual = last_generation.get_best_individual(last_generation.get_individuals())
            self.lastResult = best_individual


            self.recognize_button.config(state=tk.NORMAL)

        except cv.error:
            self.text.insert(tk.END, "Error reading the image.\n")

    # Method to delete the content of the text widget of the UI.
    def clear(self):
        self.text.delete(1.0, tk.END)


if __name__ == '__main__':
    gui = UI()
